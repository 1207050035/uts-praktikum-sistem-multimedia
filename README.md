# Praktikum Sistem Multimedia

# No 1
###### https://github.com/farrasrw/removebg-farras.git
# No 2
###### Program di atas adalah sebuah Flask web application yang bertujuan untuk memberikan fitur penghapusan latar belakang pada gambar yang diunggah oleh pengguna. Pengguna dapat mengakses aplikasi melalui browser dan mengunggah gambar melalui halaman utama. Setelah pengguna mengunggah gambar, program akan menggunakan library PIL untuk membuka dan memproses gambar tersebut. Selanjutnya, dengan menggunakan library rembg, program akan menghapus latar belakang dari gambar tersebut. Hasil gambar yang telah dihapus latar belakangnya akan disimpan dalam format PNG di direktori "static". Pengguna akan diberikan tampilan hasil gambar yang telah diproses pada halaman result.html. Jika terjadi kesalahan dalam proses pengolahan gambar, program akan menampilkan pesan error di halaman utama. Dengan demikian, program ini memberikan kemudahan bagi pengguna untuk menghapus latar belakang gambar secara otomatis melalui antarmuka web.

##### Library yang digunakan :
##### 1. rembg -> Library ini digunakan untuk pengolahan gambar dan komputer visi.
##### 2. Pillow -> Untuk membuka dan memproses gambar.
##### 3. Flask -> Flask disini sebagai framework yang berfungsi untuk mengatur rute dan menangani permintaan HTTP dari pengguna.

###### Algoritma program Image :

```mermaid
graph TD;
A[Start] --> B[upload image original]
B --> C[image processing using rembg]
C --> D[Remove Background]
D --> E[End]
```

###### Berikut demo programnya :
![demo-image](https://gitlab.com/1207050035/uts-praktikum-sistem-multimedia/-/raw/main/RemoveBackground.gif)
###### Link Youtube : 

# No 3
###### Aspek kecerdasan yang digunakan dalam program ini adalah pengolahan citra. Fungsi remove() dari library rembg digunakan untuk menghapus latar belakang gambar. Library rembg sendiri menggunakan pendekatan berbasis AI untuk mengidentifikasi dan menghapus latar belakang gambar dengan memanfaatkan model pembelajaran mesin yang telah dilatih sebelumnya. Model tersebut memiliki kemampuan untuk mempelajari perbedaan antara objek utama dan latar belakang, dan kemudian menerapkan pengetahuannya untuk menghapus latar belakang pada gambar yang diberikan.

# No 4
###### https://github.com/farrasrw/PitchAudio-Farras.git
# No 5
###### Kode di atas adalah aplikasi Flask yang memungkinkan pengguna merekam suara dan melakukan perubahan pitch pada suara yang direkam. Menggunakan library PyAudio, wave, dan numpy, aplikasi ini dapat merekam suara pengguna dalam durasi yang ditentukan dan menyimpannya dalam file wave. Selain itu, pengguna dapat melakukan perubahan pitch pada suara yang direkam dengan memasukkan nilai pitch melalui halaman web. Hasil perubahan pitch juga disimpan dalam file wave yang dapat diunduh oleh pengguna.

##### Library yang digunakan :
##### 1. Flask ->  Framework web yang digunakan untuk membangun aplikasi web. (Menyediakan fitur dasar untuk mengatur rute dan menangani permintaan HTTP.
##### 2. PyAudio -> Library ini digunakan untuk mengakses dan mengontrol audio pada perangkat komputer.
##### 3. wave -> Library ini digunakan untuk membaca dan menulis file audio dalam format wave (WAV).
##### 4. numpy -> Library ini digunakan untuk melakukan operasi numerik pada array, termasuk manipulasi data audio.

###### Algoritma program Audio :
```mermaid
flowchart TD
    A[Halaman Utama] -->|Record| B(Rekam Suara)
    B --> C[Simpan Suara]
    C --> |Pitch Shift| D(Ubah Suara)
    D --> E(Simpan Suara)

```

###### Berikut demo programnya :
![demo-image](https://gitlab.com/1207050035/uts-praktikum-sistem-multimedia/-/raw/main/Pitch_Shifter.gif)
###### Link Youtube : 
# No 6
###### Dalam konteks ini, konsep kecerdasan buatan (AI) dapat dikaitkan dengan pitch shifter dengan memanfaatkan algoritma pengenalan dan analisis suara yang canggih. Dengan menggunakan teknik pembelajaran mesin, seperti pengolahan sinyal suara dan pengenalan pola, sistem pitch shifter dapat secara otomatis mengidentifikasi dan menyesuaikan pitch suara yang direkam berdasarkan karakteristik dan konteks audio yang dianalisis. Hal ini memungkinkan program untuk secara adaptif menyesuaikan pitch suara sesuai dengan preferensi pengguna atau mengoptimalkan hasil pitch shifting secara real-time berdasarkan analisis AI yang cerdas. Dengan menggabungkan teknik AI dalam pitch shifter, aplikasi ini dapat memberikan pengalaman pengguna yang lebih canggih, responsif, dan personalisasi dalam mengubah pitch suara.
